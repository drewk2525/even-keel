from flask import json, jsonify
import random
import hashlib
import logging
import math
from database import db
from datetime import datetime
from sqlalchemy import literal, Float, update, and_, or_, not_
import os, os.path

if not os.path.exists("static/logs/"):
    os.makedirs("static/logs/")

logger = logging.getLogger('even_keel')
hdlr = logging.FileHandler('even_keel.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)


def create_salt():
    alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    chars = []
    for i in range(128):
        chars.append(random.choice(alphabet))

    return "".join(chars)


def time_convert_to_seconds(s=0, m=0, h=0):
    s = 0 if s is None else s
    m = 0 if m is None else m
    h = 0 if h is None else h
    return (float(h) * 3600) + (float(m) * 60) + float(s)


# user being passed in here is user data from the form
def check_login(user):
    # accessing the email of that user from form
    user_email = user['email']
    check_user = db.session.query(Users.UserID, Users.FirstName, Users.LastName,
                                 Users.Email, Users.Salt, Users.Password,
                                 Users.UserTypeID,
                                 LKUserType.UserTypeDescription).join(
        LKUserType).filter(Users.Email == user_email).first()
    if check_user is None:
        # when returning bools, change to json.dumps(bool)
        logger.warning("Failed login attempt, user does not exist: %s",
                       user_email)
        return json.dumps(False)

    hashed_pw = hashlib.sha512((user['password'] + check_user.Salt).encode(
            'utf-8')).hexdigest()
    if hashed_pw == check_user.Password:
        return jsonify(
            firstName=check_user.FirstName,
            lastName=check_user.LastName,
            email=check_user.Email,
            userTypeID=check_user.UserTypeID,
            userTypeDescription=check_user.UserTypeDescription
        )
    else:
        logger.warning("Failed login attempt, bad password: %s", user_email)
        return json.dumps(False)


# User Table
class Users(db.Model):
    __table__ = db.Table('Users', db.metadata, autoload=True,
                         autoload_with=db.engine)

    def __init__(self):
        """init method to declare variables used in Users class.
        """
        self.UserTypeID = None
        self.FirstName = None
        self.LastName = None
        self.Email = None
        self.Salt = None
        self.Password = None

    def add_user(self, new_user):
        """Create a new user in the database.  
        new_user is a JSON object pulled in from the front end.
        """
        self.UserTypeID = new_user['userTypeID']
        self.FirstName = new_user['firstName']
        self.LastName = new_user['lastName']
        self.Email = new_user['email']
        password = new_user['password']
        self.Salt = create_salt()
        self.Password = hashlib.sha512((password + self.Salt).encode(
            'utf-8')).hexdigest()
        try:
            db.session.add(self)
            db.session.commit()
        except Exception as e:
            return_value = "Failed to add new user, please try again later"
            logger.exception(e)
        else:
            return_value = "Success"
        return return_value

    # user being passed in here is user data from the form
    @staticmethod
    def get_user_id(user):
        # accessing the email of that user from form
        user_email = user['email']
        check_user = db.session.query(Users.UserID, Users.Salt,
                                      Users.Password).filter(
            Users.Email == user_email).first()
        if check_user is None:
            # when returning bools, change to json.dumps(bool)
            logger.warning("Checking for user failed, user does not exist: %s",
                           user_email)
            return json.dumps(False)

        hashed_pw = hashlib.sha512((user['password'] +
                                   check_user.Salt).encode(
            'utf-8')).hexdigest()
        if hashed_pw == check_user.Password:
            return check_user.UserID
        else:
            logger.warning("Checking for user failed, bad password: %s",
                           user_email)
            return json.dumps(False)


# LKHeartRateType Table
class LKHeartRateType(db.Model):
    __table__ = db.Table('LKHeartRateType', db.metadata, autoload=True,
                         autoload_with=db.engine)

    @staticmethod
    def get_heart_rate_types_json():
        hr_types = db.session.query(LKHeartRateType.HeartRateTypeID,
                                    LKHeartRateType.HeartRateTypeDescription).order_by(
            LKHeartRateType.HeartRateTypeID).all()
        return jsonify([r._asdict() for r in hr_types])

    @staticmethod
    def add_heart_rate(hr, user_id):
        bpm = hr['heartRateValue']
        heart_rate_type_id = hr['heartRateType']
        heart_rate = HeartRates(HeartRateTypeID=heart_rate_type_id, BPM=bpm,
                               UserID=user_id, CreateDT=datetime.utcnow())
        db.session.add(heart_rate)
        db.session.commit()
        return json.dumps(True)


# LKUserType Table
class LKUserType(db.Model):
    __table__ = db.Table('LKUserType', db.metadata, autoload=True,
                         autoload_with=db.engine)

    @staticmethod
    def get_user_types():
        user_types = db.session.query.order_by(LKUserType.UserTypeID)
        return user_types


# LKWorkoutType Table
class LKWorkoutType(db.Model):
    __table__ = db.Table('LKWorkoutType', db.metadata, autoload=True,
                         autoload_with=db.engine)

    @staticmethod
    def get_workout_types_json(user_id=0):
        user_type = db.session.query(Users.UserTypeID).filter(Users.UserID ==
                                                              user_id).one()
        workout_types = db.session.query(
            LKWorkoutType.WorkoutTypeID,
            LKWorkoutType.WorkoutTypeDescription).filter(
             or_(LKWorkoutType.UserType == user_type[0],
                 user_type[0] == 0, LKWorkoutType.UserType == None)
             ).order_by(LKWorkoutType.WorkoutTypeID).all()
        return jsonify([r._asdict() for r in workout_types])


# LKWorkoutSetType Table
class LKWorkoutSetType(db.Model):
    __table__ = db.Table('LKWorkoutSetType', db.metadata, autoload=True,
                         autoload_with=db.engine)

    @staticmethod
    def get_workout_set_types_json(user_id=0):
        user_type = db.session.query(Users.UserTypeID).filter(
            Users.UserID ==
            user_id).one()
        workout_types = db.session.query(
            LKWorkoutSetType.WorkoutSetTypeID,
            LKWorkoutSetType.WorkoutSetTypeDescription).filter(
            or_(LKWorkoutSetType.OwnerTypeID == user_type[0],
                user_type[0] == 0, LKWorkoutSetType.OwnerTypeID == None)
        ).order_by(LKWorkoutSetType.WorkoutSetTypeID).all()
        return jsonify([r._asdict() for r in workout_types])


# LKDistanceType Table
class LKDistanceType(db.Model):
    __table__ = db.Table('LKDistanceType', db.metadata, autoload=True,
                         autoload_with=db.engine)

    @staticmethod
    def get_distance_types_json(user_id=0):
        user_type = db.session.query(Users.UserTypeID).filter(
            Users.UserID ==
            user_id).one()
        distance_types = db.session.query(
            LKDistanceType.DistanceTypeID,
            LKDistanceType.DistanceTypeDescription,
            LKDistanceType.DistanceTypeUnits).filter(
            or_(LKDistanceType.OwnerTypeID == user_type[0],
                user_type[0] == 0, LKWorkoutSetType.OwnerTypeID == None)
        ).order_by(LKDistanceType.DistanceTypeID).all()
        return jsonify([r._asdict() for r in distance_types])


# HeartRates Table
class HeartRates(db.Model):
    __table__ = db.Table('HeartRates', db.metadata, autoload=True,
                         autoload_with=db.engine)

    def __init__(self):
        self.BPM = None
        self.HeartRateTypeID = None
        self.UserID = None
        self.CreateDT = None

    def add_heart_rate(self, hr, user_id):
        self.BPM = hr['heartRateValue']
        self.HeartRateTypeID = hr['heartRateType']
        self.UserID = user_id
        self.CreateDT = datetime.utcnow()
        db.session.add(self)
        db.session.commit()
        return json.dumps(True)

    @staticmethod
    def get_user_heart_rates(user_id):
        user_heart_rates = db.session.query(HeartRates.HeartRateID,
                                            LKHeartRateType.HeartRateTypeDescription,
                                            HeartRates.BPM,
                                            HeartRates.CreateDT).join(
            LKHeartRateType).filter(
            HeartRates.UserID == user_id).order_by(
            HeartRates.CreateDT.desc()).all()
        return jsonify([r._asdict() for r in user_heart_rates])


# Workouts table
class WorkoutActivities(db.Model):
    __table__ = db.Table('WorkoutActivities', db.metadata, autoload=True,
                         autoload_with=db.engine)
    
    def __init__(self):
        self.WorkoutID = None
        self.WorkoutActivityDescription = None

    def add_workout_activity(self, workout_activity):
        self.WorkoutID = workout_activity['workoutID']
        self.WorkoutActivityDescription = workout_activity[
            'workoutActivityDescription']
        db.session.add(self)
        db.session.commit()
        return json.dumps(True)

    @staticmethod
    def get_user_workout_activities(user_id):
        user_workout_activities = db.session.query(
            WorkoutActivities.WorkoutActivityID,
                                         WorkoutActivities.WorkoutID,
                                         WorkoutActivities.WorkoutActivityDescription,
                                         WorkoutActivities.RequestCoachEndorsementDT,
                                         WorkoutActivities.CoachEndorsementDT,
                                         WorkoutActivities.EndorsedByUserID).join(
            Workouts).filter(
            Workouts.UserID == user_id).all()
        return jsonify([r._asdict() for r in user_workout_activities])

    @staticmethod
    def update_workout_activity(workout_activity):
        workout_activity_id = workout_activity['WorkoutActivityID']
        workout_activity_description = workout_activity[
            'WorkoutActivityDescription']

        user_workout_activity = db.session.query(
            WorkoutActivities.WorkoutActivityID).filter(
            WorkoutActivities.WorkoutActivityID == workout_activity_id).update(
            values={
            "WorkoutActivityDescription": workout_activity_description})
        db.session.commit()

        if user_workout_activity >= 1:
            return json.dumps(True)
        else:
            return json.dumps(False)

    @staticmethod
    def check_user_workout_activity(user_id, workout_activity_id):
        user_workout_activity = db.session.query(
            WorkoutActivities.WorkoutActivityID).join(Workouts).filter(
            Workouts.UserID == user_id).filter(
            WorkoutActivities.WorkoutActivityID == workout_activity_id).all()
        if user_workout_activity:
            return_value = True
        else:
            logger.warning("WorkoutActivityID: %s does not belong to UserID: "
                           "%s", workout_activity_id, user_id)
            return_value = False
        return return_value


class WorkoutSets(db.Model):
    __table__ = db.Table('WorkoutSets', db.metadata, autoload=True,
                         autoload_with=db.engine)

    def __init__(self):
        self.WorkoutSetID = None
        self.WorkoutActivityID = None
        self.WorkoutSetTypeID = None
        self.WorkoutSetDescription = None
        self.Weight = None
        self.Reps = None
        self.ExerciseTimeHours = None
        self.ExerciseTimeMinutes = None
        self.ExerciseTimeSeconds = None
        # self.ExerciseTime = None
        # self.SplitTime = None
        self.SplitTimeMinutes = None
        self.SplitTimeSeconds = None
        self.SPM = None
        # self.RestTime = None
        self.RestTimeMinutes = None
        self.RestTimeSeconds = None
        self.DesiredIntensity = None
        self.DesiredIntensityTypeID = None
        self.HeartRateID = None
        self.DistanceTypeID = None
        self.Distance = None

    def add_workout_set(self, workout_set):
        eth = workout_set['workoutSetExerciseTimeHours']
        etm = workout_set['workoutSetExerciseTimeMinutes']
        ets = workout_set['workoutSetExerciseTimeSeconds']
        stm = workout_set['workoutSetSplitTimeMinutes']
        sts = workout_set['workoutSetSplitTimeSeconds']
        rtm = workout_set['workoutSetRestTimeMinutes']
        rts = workout_set['workoutSetRestTimeSeconds']
        # # shorthand to make calculations easier to read
        # # h, m, s is for exercise time hours minutes seconds
        # h = workout_set['workoutSetExerciseTimeHours']
        # h = 0 if h is None else h
        # m = workout_set['workoutSetExerciseTimeMinutes']
        # m = 0 if m is None else m
        # s = workout_set['workoutSetExerciseTimeSeconds']
        # s = 0 if s is None else s
        # # sm, ss is for split minutes and split seconds
        # sm = workout_set['workoutSetSplitTimeMinutes']
        # sm = 0 if sm is None else sm
        # ss = workout_set['workoutSetSplitTimeSeconds']
        # ss = 0 if ss is None else ss
        # # rm, rs is for rest minutes and rest seconds
        # rm = workout_set['workoutSetRestTimeMinutes']
        # rm = 0 if rm is None else rm
        # rs = workout_set['workoutSetRestTimeSeconds']
        # rs = 0 if rs is None else rs

        self.WorkoutActivityID = workout_set['workoutSetWorkoutActivityID']
        self.WorkoutSetTypeID = workout_set['workoutSetTypeID']
        self.WorkoutSetDescription = workout_set['workoutSetDescription']
        self.Weight = workout_set['workoutSetWeight']
        self.Reps = workout_set['workoutSetReps']
        self.ExerciseTime = time_convert_to_seconds(ets, etm, eth)
        # (float(h)*3600) + (float(m)*60) + float(s)
        self.SplitTime = time_convert_to_seconds(sts, stm)
        # (float(sm) * 60) + float(ss)
        self.SPM = None if workout_set['workoutSetSPM'] is None else int(
            workout_set['workoutSetSPM'])
        self.RestTime = time_convert_to_seconds(rts, rtm)
        # (float(rm)*60) + float(rs)
        self.DistanceTypeID = workout_set['workoutSetDistanceTypeID']
        self.Distance = workout_set['workoutSetDistance']
        db.session.add(self)
        db.session.commit()
        return json.dumps(True)

    @staticmethod
    def get_user_workout_sets(user_id):
        user_workout_sets = db.session.query(WorkoutSets.WorkoutSetID,
                                             WorkoutSets.WorkoutActivityID,
                                             WorkoutSets.WorkoutSetTypeID,
                                             WorkoutSets.WorkoutSetDescription,
                                             WorkoutSets.Weight,
                                             WorkoutSets.Reps,
                                             WorkoutSets.Distance,
                                             LKDistanceType.DistanceTypeUnits,
                                             LKDistanceType.DistanceTypeDescription,
                                             WorkoutSets.ExerciseTime,
                                             WorkoutSets.SplitTime,
                                             WorkoutSets.SPM,
                                             WorkoutSets.RestTime,
                                             WorkoutSets.DesiredIntensity,
                                             WorkoutSets.DesiredIntensityTypeID,
                                             WorkoutSets.HeartRateID,
                                             LKWorkoutSetType.WorkoutSetTypeDescription).join(
            WorkoutActivities).join(Workouts).join(LKWorkoutSetType).join(
            LKDistanceType).filter(Workouts.UserID == user_id).all()

        return jsonify([r._asdict() for r in user_workout_sets])
        # return jsonify(user_workout_sets)
        # return json.dumps(user_workout_sets, default=json_util.default)

    @staticmethod
    def update_workout_set(workout_set):
        eth = workout_set['ExerciseTimeHours']
        etm = workout_set['ExerciseTimeMinutes']
        ets = workout_set['ExerciseTimeSeconds']
        stm = workout_set['SplitTimeMinutes']
        sts = workout_set['SplitTimeSeconds']
        rtm = workout_set['RestTimeMinutes']
        rts = workout_set['RestTimeSeconds']
        # workout_set_exercise_time = workout_set['ExerciseTime']
        # workout_set_split_time = workout_set['SplitTime']
        # workout_set_rest_time = workout_set['RestTime']
        workout_set_id = workout_set['WorkoutSetID']
        workout_set_type_id = workout_set['WorkoutSetTypeID']
        workout_set_description = workout_set['WorkoutSetDescription']
        workout_set_weight = workout_set['Weight']
        workout_set_reps = workout_set['Reps']
        workout_set_distance = workout_set['Distance']
        # workout_set_distance_type_id = workout_set['DistanceTypeID']
        workout_set_spm = workout_set['SPM']
        workout_set_desired_intensity = workout_set[
            'DesiredIntensity']
        workout_set_desired_intensity_type_id = workout_set[
            'DesiredIntensityTypeID']
        workout_set_heart_rate_id = workout_set['HeartRateID']

        user_workout_set = db.session.query(WorkoutSets.WorkoutSetID).filter(
            WorkoutSets.WorkoutSetID == workout_set_id).update(
            values={
                "WorkoutSetTypeID": workout_set_type_id,
                "WorkoutSetDescription": workout_set_description,
                "Weight": workout_set_weight,
                "Reps": workout_set_reps,
                "ExerciseTime": time_convert_to_seconds(ets, etm,
                                                                  eth),
                "Distance": workout_set_distance,
                # "WorkoutSetDistanceTypeID": workout_set_distance_type_id,
                "SplitTime": time_convert_to_seconds(sts, stm),
                "SPM": workout_set_spm,
                "RestTime": time_convert_to_seconds(rts, rtm),
                "DesiredIntensity": workout_set_desired_intensity,
                "DesiredIntensityTypeID":
                    workout_set_desired_intensity_type_id,
                "HeartRateID": workout_set_heart_rate_id})
        db.session.commit()

        if user_workout_set >= 1:
            return json.dumps(True)
        else:
            return json.dumps(False)

    @staticmethod
    def check_user_workout_set(user_id, workout_set_id):
        user_workout_set = db.session.query(
            WorkoutSets.WorkoutSetID).join(WorkoutActivities).join(Workouts)\
            .filter(Workouts.UserID == user_id)\
            .filter(WorkoutSets.WorkoutSetID == workout_set_id).all()
        if user_workout_set:
            return_value = True
        else:
            logger.warning("WorkoutSetID: %s does not "
                           + "belong to UserID: %s", workout_set_id, user_id)
            return_value = False
        return return_value


class Workouts(db.Model):
    __table__ = db.Table('Workouts', db.metadata, autoload=True,
                         autoload_with=db.engine)

    def __init__(self):
        self.WorkoutTypeID = None
        self.UserID = None
        self.WorkoutDescription = None
        self.CreateDT = None

    def add_workout(self, workout, user_id):
        self.WorkoutTypeID = workout['workoutTypeID']
        self.UserID = user_id
        self.WorkoutDescription = workout[
            'workoutDescription']
        self.CreateDT = datetime.utcnow()
        db.session.add(self)
        db.session.commit()
        return json.dumps(True)

    @staticmethod
    def update_workout(workout):
        workout_id = workout['WorkoutID']
        workout_description = workout['WorkoutDescription']

        user_workout = db.session.query(Workouts.WorkoutID).filter(
            Workouts.WorkoutID == workout_id).update(values={
                "WorkoutDescription": workout_description})
        db.session.commit()

        if user_workout >= 1:
            return json.dumps(True)
        else:
            return json.dumps(False)


    @staticmethod
    def get_user_workouts(user_id):
        user_workouts = db.session.query(
            Workouts.WorkoutID,
            Workouts.WorkoutTypeID,
            Workouts.WorkoutDescription,
            Workouts.CreateDT,
            LKWorkoutType.WorkoutTypeDescription
        ).join(LKWorkoutType).filter(Workouts.UserID ==
                                          user_id).all()
        return jsonify([r._asdict() for r in user_workouts])

    @staticmethod
    def check_user_workout(user_id, workout_id):
        user_workout = db.session.query(
            Workouts.WorkoutID).filter(Workouts.UserID ==
                                                    user_id).filter(
            Workouts.WorkoutID == workout_id).all()
        if user_workout:
            return_value = True
        else:
            logger.warning("User has no workouts, UserID: %s", user_id)
            return_value = False
        return return_value


class Notes(db.Model):
    __table__ = db.Table('Notes', db.metadata, autoload=True,
                         autoload_with=db.engine)

    def __init__(self):
        self.NoteID = None
        self.SystemAreaTypeID = None
        self.SystemAreaID = None
        self.NoteContent = None
        self.CreatedByUserID = None
        self.ForUserID = None
        self.CreateDT = None

    def add_note(self, user_id, system_area_type_id, note_details):
        self.SystemAreaTypeID = system_area_type_id
        self.SystemAreaID = note_details['systemAreaID']
        self.CreatedByUserID = user_id
        if note_details['forUserID'] == 0:
            note_details['forUserID'] = user_id
        self.ForUserID = note_details['forUserID']
        self.CreateDT = datetime.utcnow()
        self.NoteContent = note_details['noteContent']
        db.session.add(self)
        db.session.commit()
        return json.dumps(True)

    @staticmethod
    def get_notes(user_id, system_area_type_id, system_area_id):
        notes = db.session.query(
            Notes.NoteID,
            Notes.SystemAreaTypeID,
            Notes.SystemAreaID,
            Notes.NoteContent,
            Notes.CreateDT,
            Users.FirstName,
            Users.LastName
        ).join(Users, Users.UserID == Notes.CreatedByUserID).filter(
            Notes.ForUserID == user_id).filter(Notes.SystemAreaTypeID ==
                                               system_area_type_id).filter(
            Notes.SystemAreaID == system_area_id).all()
        return jsonify([r._asdict() for r in notes])


class Organizations(db.Model):
    __table__ = db.Table('Organizations', db.metadata, autoload=True,
                         autoload_with=db.engine)

    def __init__(self):
        """init method to declare variables used in Organizations class.
        """
        self.OrganizationName = None
        self.OrganizationEmail = None
        self.OrganizationSalt = None
        self.OrganizationPassword = None

    def add_organization(self, new_organization):
        """Create a new user in the database.  
        new_user is a JSON object pulled in from the front end.
        """
        self.OrganizationName = new_organization['organizationName']
        self.OrganizationEmail = new_organization['organizationEmail']
        password = new_organization['organizationPassword']
        self.OrganizationSalt = create_salt()
        self.OrganizationPassword = hashlib.sha512((password +
                                                    self.OrganizationSalt).encode(
            'utf-8')).hexdigest()
        try:
            db.session.add(self)
            db.session.commit()
        except Exception as e:
            return_value = "Failed to add new user, please try again later"
            logger.exception(e)
        else:
            return_value = "Success"
        return return_value