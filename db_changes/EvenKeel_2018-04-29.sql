-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: EvenKeel
-- ------------------------------------------------------
-- Server version	5.5.5-10.2.7-MariaDB-10.2.7+maria~xenial-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AthleteStatHistory`
--

DROP TABLE IF EXISTS `AthleteStatHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AthleteStatHistory` (
  `AthleteStatHistoryID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UserID` int(11) unsigned NOT NULL,
  `AthleteStatTypeID` int(11) unsigned NOT NULL,
  `Value` float NOT NULL,
  `CreateDT` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`AthleteStatHistoryID`),
  KEY `UserID` (`UserID`),
  KEY `AthleteStatTypeID` (`AthleteStatTypeID`),
  CONSTRAINT `athletestathistory_ibfk_3` FOREIGN KEY (`UserID`) REFERENCES `Users` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `athletestathistory_ibfk_4` FOREIGN KEY (`AthleteStatTypeID`) REFERENCES `LKAthleteStatType` (`AthleteStatTypeID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HeartRates`
--

DROP TABLE IF EXISTS `HeartRates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HeartRates` (
  `HeartRateID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `HeartRateTypeID` int(11) unsigned NOT NULL,
  `BPM` int(3) unsigned NOT NULL,
  `UserID` int(11) unsigned NOT NULL,
  `CreateDT` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`HeartRateID`),
  KEY `HeartRateTypeID` (`HeartRateTypeID`),
  KEY `UserID` (`UserID`),
  CONSTRAINT `heartrates_ibfk_3` FOREIGN KEY (`HeartRateTypeID`) REFERENCES `LKHeartRateType` (`HeartRateTypeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `heartrates_ibfk_4` FOREIGN KEY (`UserID`) REFERENCES `Users` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LKAthleteStatType`
--

DROP TABLE IF EXISTS `LKAthleteStatType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LKAthleteStatType` (
  `AthleteStatTypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `AthleteStatTypeDescription` varchar(60) NOT NULL DEFAULT '',
  PRIMARY KEY (`AthleteStatTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LKDesiredIntensityType`
--

DROP TABLE IF EXISTS `LKDesiredIntensityType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LKDesiredIntensityType` (
  `DesiredIntensityTypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `DesiredIntensityTypeDescription` varchar(60) NOT NULL DEFAULT '',
  `OwnerTypeID` int(11) unsigned NOT NULL,
  `OwnerID` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`DesiredIntensityTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LKDistanceType`
--

DROP TABLE IF EXISTS `LKDistanceType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LKDistanceType` (
  `DistanceTypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `DistanceTypeDescription` varchar(32) NOT NULL DEFAULT '',
  `DistanceTypeUnits` varchar(10) NOT NULL DEFAULT '',
  `OwnerTypeID` int(11) unsigned NOT NULL DEFAULT 1,
  `OwnerID` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`DistanceTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LKHeartRateType`
--

DROP TABLE IF EXISTS `LKHeartRateType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LKHeartRateType` (
  `HeartRateTypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `HeartRateTypeDescription` varchar(60) NOT NULL DEFAULT '',
  PRIMARY KEY (`HeartRateTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LKOwnerType`
--

DROP TABLE IF EXISTS `LKOwnerType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LKOwnerType` (
  `OwnerTypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `OwnerTypeDescription` varchar(60) NOT NULL DEFAULT '',
  PRIMARY KEY (`OwnerTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LKUserType`
--

DROP TABLE IF EXISTS `LKUserType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LKUserType` (
  `UserTypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UserTypeDescription` varchar(60) NOT NULL DEFAULT '',
  PRIMARY KEY (`UserTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LKWorkoutSetType`
--

DROP TABLE IF EXISTS `LKWorkoutSetType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LKWorkoutSetType` (
  `WorkoutSetTypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `WorkoutSetTypeDescription` varchar(60) NOT NULL DEFAULT '',
  `OwnerTypeID` int(11) unsigned NOT NULL,
  `OwnerID` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`WorkoutSetTypeID`),
  KEY `OwnerID` (`OwnerID`),
  KEY `OwnerTypeID` (`OwnerTypeID`),
  CONSTRAINT `lkworkoutsettype_ibfk_4` FOREIGN KEY (`OwnerID`) REFERENCES `Users` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `lkworkoutsettype_ibfk_6` FOREIGN KEY (`OwnerTypeID`) REFERENCES `LKOwnerType` (`OwnerTypeID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LKWorkoutType`
--

DROP TABLE IF EXISTS `LKWorkoutType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LKWorkoutType` (
  `WorkoutTypeID` int(11) unsigned NOT NULL,
  `WorkoutTypeDescription` varchar(60) DEFAULT NULL,
  `UserType` int(11) DEFAULT NULL,
  PRIMARY KEY (`WorkoutTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Notes`
--

DROP TABLE IF EXISTS `Notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Notes` (
  `NoteID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SystemAreaTypeID` int(11) unsigned NOT NULL,
  `SystemAreaID` int(11) unsigned NOT NULL,
  `NoteContent` text NOT NULL,
  `CreatedByUserID` int(11) unsigned NOT NULL,
  `ForUserID` int(11) unsigned NOT NULL,
  `CreateDT` datetime NOT NULL,
  PRIMARY KEY (`NoteID`),
  KEY `SystemAreaTypeID` (`SystemAreaTypeID`),
  KEY `CreatedByUserID` (`CreatedByUserID`),
  KEY `ForUserID` (`ForUserID`),
  CONSTRAINT `notes_ibfk_1` FOREIGN KEY (`SystemAreaTypeID`) REFERENCES `SystemAreaType` (`SystemAreaTypeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `notes_ibfk_2` FOREIGN KEY (`CreatedByUserID`) REFERENCES `Users` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `notes_ibfk_3` FOREIGN KEY (`ForUserID`) REFERENCES `Users` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Organizations`
--

DROP TABLE IF EXISTS `Organizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Organizations` (
  `OrganizationID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `OrganizationName` varchar(60) DEFAULT NULL,
  `OrganizationEmail` varchar(256) DEFAULT NULL,
  `OrganizationSalt` char(128) DEFAULT NULL,
  `OrganizationPassword` char(128) DEFAULT NULL,
  PRIMARY KEY (`OrganizationID`),
  UNIQUE KEY `OrganizationEmail_UNIQUE` (`OrganizationEmail`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SystemAreaType`
--

DROP TABLE IF EXISTS `SystemAreaType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SystemAreaType` (
  `SystemAreaTypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SystemAreaTypeDescription` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`SystemAreaTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `UserID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UserTypeID` int(11) unsigned DEFAULT NULL,
  `FirstName` varchar(60) DEFAULT NULL,
  `LastName` varchar(60) DEFAULT NULL,
  `Email` varchar(256) DEFAULT NULL,
  `Salt` char(128) DEFAULT NULL,
  `Password` char(128) DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `Email` (`Email`),
  KEY `UserTypeID` (`UserTypeID`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`UserTypeID`) REFERENCES `LKUserType` (`UserTypeID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WorkoutActivities`
--

DROP TABLE IF EXISTS `WorkoutActivities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WorkoutActivities` (
  `WorkoutActivityID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `WorkoutID` int(11) unsigned NOT NULL,
  `WorkoutActivityDescription` text DEFAULT NULL,
  `RequestCoachEndorsementDT` datetime DEFAULT NULL,
  `CoachEndorsementDT` datetime DEFAULT NULL,
  `EndorsedByUserID` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`WorkoutActivityID`),
  KEY `EndorsedByUserID` (`EndorsedByUserID`),
  KEY `WorkoutSessionID` (`WorkoutID`),
  CONSTRAINT `workoutactivities_ibfk_4` FOREIGN KEY (`EndorsedByUserID`) REFERENCES `Users` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `workoutactivities_ibfk_5` FOREIGN KEY (`WorkoutID`) REFERENCES `Workouts` (`WorkoutID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WorkoutSets`
--

DROP TABLE IF EXISTS `WorkoutSets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WorkoutSets` (
  `WorkoutSetID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `WorkoutActivityID` int(11) unsigned NOT NULL,
  `WorkoutSetTypeID` int(11) unsigned NOT NULL,
  `Weight` float DEFAULT NULL,
  `Reps` smallint(6) DEFAULT NULL,
  `ExerciseTime` float DEFAULT NULL,
  `Distance` int(11) DEFAULT NULL,
  `DistanceTypeID` int(11) unsigned NOT NULL DEFAULT 1,
  `SplitTime` float DEFAULT NULL,
  `SPM` tinyint(3) DEFAULT NULL,
  `RestTime` float DEFAULT NULL,
  `DesiredIntensity` tinyint(3) DEFAULT NULL,
  `DesiredIntensityTypeID` int(11) unsigned DEFAULT NULL,
  `HeartRateID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`WorkoutSetID`),
  KEY `WorkoutSetTypeID` (`WorkoutSetTypeID`),
  KEY `WorkoutID` (`WorkoutActivityID`),
  KEY `DistanceTypeID` (`DistanceTypeID`),
  CONSTRAINT `workoutsets_ibfk_2` FOREIGN KEY (`WorkoutSetTypeID`) REFERENCES `LKWorkoutSetType` (`WorkoutSetTypeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `workoutsets_ibfk_3` FOREIGN KEY (`WorkoutActivityID`) REFERENCES `WorkoutActivities` (`WorkoutActivityID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `workoutsets_ibfk_4` FOREIGN KEY (`DistanceTypeID`) REFERENCES `LKDistanceType` (`DistanceTypeID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Workouts`
--

DROP TABLE IF EXISTS `Workouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Workouts` (
  `WorkoutID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `WorkoutTypeID` int(11) unsigned NOT NULL,
  `WorkoutDescription` text DEFAULT NULL,
  `UserID` int(11) unsigned NOT NULL,
  `CreateDT` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`WorkoutID`),
  KEY `WorkoutSessionTypeID` (`WorkoutTypeID`),
  KEY `UserID` (`UserID`),
  CONSTRAINT `workouts_ibfk_3` FOREIGN KEY (`WorkoutTypeID`) REFERENCES `LKWorkoutType` (`WorkoutTypeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `workouts_ibfk_4` FOREIGN KEY (`UserID`) REFERENCES `Users` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping events for database 'EvenKeel'
--

--
-- Dumping routines for database 'EvenKeel'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-29 12:39:38
