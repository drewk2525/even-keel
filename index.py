from flask import render_template, request, jsonify, json, session
from flask_session import Session
# from flask_sqlalchemy import *
# from flask.ext.session import Session
from database import app, db
from dataModel import Users, check_login, HeartRates, LKHeartRateType, \
    WorkoutActivities, Workouts, WorkoutSets, LKWorkoutType, \
    LKWorkoutSetType, LKDistanceType, Notes, Organizations
import jinja2

app.debug = True
app.secret_key = 'super-secret'
app.config['SESSION_TYPE'] = 'filesystem'
# 14400 = 4 hour session lifetime
app.config['PERMANENT_SESSION_LIFETIME'] = 14400

Session(app)


@app.route("/")
def home():
    return render_template('home.html')


@app.route("/api/newUser", methods=['POST'])
def create_new_user():
    if request.method == 'POST':
        new_user = request.get_json(force=True, silent=True)
        new_user_result = Users().add_user(new_user)
        return jsonify(
            new_user_result
        )
    else:
        return json.dumps(False)


@app.route("/api/login", methods=['POST'])
def login():
    if request.method == 'POST':
        user = request.get_json(force=True, silent=True)
        session['user'] = check_login(user)
        if session['user'] != "false":
            session['isValid'] = 'True'
            session['userID'] = Users.get_user_id(user)
            return session['user']
        else:
            session.clear()
            return json.dumps(False)
    else:
        return json.dumps(False)


@app.route("/api/logout", methods=['POST'])
def logout():
    session.clear()
    return json.dumps(True)

@app.route("/api/newOrganization", methods=['POST'])
def create_new_organization():
    if request.method == 'POST':
        new_organization = request.get_json(force=True, silent=True)
        new_organization_result = Organizations().add_organization(new_organization)
        return jsonify(
            new_organization_result
        )
    else:
        return json.dumps(False)

@app.route("/api/addWorkoutNote", methods=['POST'])
def add_workout_note():
    if request.method == 'POST':
        note_details = request.get_json(force=True, silent=True)
        user_id = session['userID']
        note_result = Notes().add_note(user_id, 1, note_details)
        return note_result
    else:
        return json.dumps(False)


@app.route("/api/getWorkoutNotes", methods=['POST'])
def get_workout_notes():
    if request.method == 'POST':
        if request.method == 'POST':
            workout_notes = request.get_json(force=True, silent=True)
            return Notes.get_notes(session['userID'], 1, workout_notes[
                'workoutID'])
    else:
        return json.dumps(False)


@app.route("/api/checkValidSession", methods=['POST'])
def check_valid_session():
    return session.get('user', 'false')


@app.route("/api/getHeartRateTypes", methods=['POST'])
def hr_types():
    if request.method == 'POST':
        heart_rate_types = LKHeartRateType.get_heart_rate_types_json()
        return heart_rate_types
    else:
        return json.dumps(False)


@app.route("/api/getWorkoutTypes", methods=['POST'])
def workout_types():
    if request.method == 'POST':
        return LKWorkoutType.get_workout_types_json(session[
                                                                       'userID'])
    else:
        return json.dumps(False)


@app.route("/api/getWorkoutSetTypes", methods=['POST'])
def workout_set_types():
    if request.method == 'POST':
        return LKWorkoutSetType.get_workout_set_types_json(session['userID'])
    else:
        return json.dumps(False)


@app.route("/api/getDistanceTypes", methods=['POST'])
def distance_types():
    if request.method == 'POST':
        return LKDistanceType.get_distance_types_json(session['userID'])
    else:
        return json.dumps(False)


@app.route("/api/getUserHeartRates", methods=['POST'])
def user_heart_rates():
    if request.method == 'POST':
        return HeartRates.get_user_heart_rates(session['userID'])
    else:
        return json.dumps(False)


@app.route("/api/addHeartRate", methods=['POST'])
def add_hr():
    if request.method == 'POST':
        hr = request.get_json(force=True, silent=True)
        user_id = session['userID']
        hr_result = HeartRates().add_heart_rate(hr, user_id)
        return hr_result
    else:
        return json.dumps(False)


@app.route("/api/addWorkout", methods=['POST'])
def add_workout():
    if request.method == 'POST':
        workout = request.get_json(force=True, silent=True)
        workout_result = Workouts().add_workout(
            workout, session['userID'])
        return workout_result
    else:
        return json.dumps(False)


@app.route("/api/updateWorkout", methods=['POST'])
def update_workout():
    if request.method == 'POST':
        workout = request.get_json(force=True, silent=True)
        if (Workouts().check_user_workout(session['userID'],
                                          workout['WorkoutID'])):
            workout_result = Workouts().update_workout(workout)
            return workout_result
        else:
            return json.dumps(False)
    else:
        return json.dumps(False)


@app.route("/api/addWorkoutActivity", methods=['POST'])
def add_workout_activity():
    if request.method == 'POST':
        workout_activity = request.get_json(force=True, silent=True)
        if (Workouts().check_user_workout(session['userID'], workout_activity[
                                                            'workoutID'])):
            workout_activity_result = WorkoutActivities().add_workout_activity(
                workout_activity)
            return workout_activity_result
        else:
            return json.dumps(False)
    else:
        return json.dumps(False)


@app.route("/api/updateWorkoutActivity", methods=['POST'])
def update_workout_activity():
    if request.method == 'POST':
        workout_activity = request.get_json(force=True, silent=True)
        if (WorkoutActivities().check_user_workout_activity(session['userID'],
                                                            workout_activity[
                                                       'WorkoutActivityID'])):
            workout_activity_result = WorkoutActivities()\
                .update_workout_activity(workout_activity)
            return workout_activity_result
        else:
            return json.dumps(False)
    else:
        return json.dumps(False)


@app.route("/api/addWorkoutSet", methods=['POST'])
def add_workout_set():
    if request.method == 'POST':
        workout_set = request.get_json(force=True, silent=True)
        if (Workouts().check_user_workout(session['userID'],
                                                        workout_set[
                                                            'workoutSetWorkoutID'])):
            workout_set_result = WorkoutSets().add_workout_set(workout_set)
            return workout_set_result
        else:
            return json.dumps(False)
    else:
        return json.dumps(False)


@app.route("/api/updateWorkoutSet", methods=['POST'])
def update_workout_set():
    if request.method == 'POST':
        workout_set = request.get_json(force=True, silent=True)
        if (WorkoutSets().check_user_workout_set(session['userID'],
                                                            workout_set[
                                                       'WorkoutSetID'])):
            workout_set_result = WorkoutSets()\
                .update_workout_set(workout_set)
            return workout_set_result
        else:
            return json.dumps(False)
    else:
        return json.dumps(False)

@app.route("/api/getUserWorkoutSets", methods=['POST'])
def get_user_workout_sets():
    if request.method == 'POST':
        return WorkoutSets.get_user_workout_sets(session['userID'])
    else:
        return json.dumps(False)


@app.route("/api/getUserWorkoutActivities", methods=['POST'])
def get_user_workout_activities():
    if request.method == 'POST':
        return WorkoutActivities.get_user_workout_activities(session['userID'])
    else:
        return json.dumps(False)
    

@app.route("/api/getUserWorkouts", methods=['POST'])
def get_user_workout_sessions():
    if request.method == 'POST':
        return Workouts.get_user_workouts(session['userID'])
    else:
        return json.dumps(False)


if __name__ == "__main__":
    app.run()