import axios from 'axios'

function generalAPICall(str, obj=null){
  return axios.post('/api/'+str, JSON.stringify(obj))
    .then(function(data){
      if(data['data'] == 0){
        return 0;
      }
      return data['data'];
  },
  function(error){
    return false;
  });
}
//
//function updateWorkout(workoutObject){
//  generalAPICall('updateWorkout', workoutObject).then((data) => {
//    if(!data){
//      workoutObject[index] = this.preEditWorkoutObject[index];
//      this.$parent.$emit('update-alert', "Failure", systemArea.replace(/([a-z])([A-Z])/g, '$1 $2') + " was not updated, please try again later.");
//    } else {
//      this.$parent.$emit('update-alert', "Success", systemArea.replace(/([a-z])([A-Z])/g, '$1 $2') + " was updated successfully!");
//      return true;
//    }
//  });
//}






//function getHeartRateTypes(){
//  return axios.post('/api/getHeartRateTypes')
//    .then(function(data){
//      if(data['data'] == 0){
//        return 0;
//      }
//      return data['data'];
//    });
//}
//
//function addHeartRate(HR){
//  return axios.post('/api/addHeartRate', JSON.stringify(HR))
//    .then(function(data){
//      if(data['data'] == 0){
//        return 0;
//      }
//      return data['data'];
//    });
//}

export { generalAPICall }