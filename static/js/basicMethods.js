function clearAlerts(t){
  t.alertMessage = ""
  t.alertSuccess = false;
  t.alertFailure = false;
}

function formatDateTime(date){
  var moment = require('moment');
  return moment(date).format("YYYY-MM-DD hh:mm:ss");
}

function round(number, precision) {
  var shift = function (number, precision) {
    var numArray = ("" + number).split("e");
    return +(numArray[0] + "e" + (numArray[1] ? (+numArray[1] + precision) : precision));
  };
  return shift(Math.round(shift(number, +precision)), -precision);
}

function convertTimeHMS(t){
  let hours = Math.floor(t/3600);
  let minutes = Math.floor((t - hours * 3600) / 60);
  minutes = minutes < 10 ? "0" + minutes : minutes;
  let seconds = round(10 * (t - (hours * 3600 + minutes * 60))/10, 1);
  seconds = seconds < 10 ? "0" + seconds.toFixed(1) : seconds.toFixed(1);
  return [hours, minutes, seconds];
}

function focusElement(e){
  let timer = 5;
  setTimeout(excuteMethod, 100);

  function excuteMethod() {
    let d = document.getElementById(e);
    d == null ? timer-- : d.focus(); timer = -1;
    if (timer >= 0) setTimeout(excuteMethod,  100);
  }
}


export { clearAlerts, formatDateTime, focusElement, round, convertTimeHMS }