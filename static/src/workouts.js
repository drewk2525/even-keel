import { generalAPICall, updateWorkout } from '../js/api.js'
import { formatDateTime, focusElement, convertTimeHMS } from '../js/basicMethods.js'

export default {
  name: 'app',
  data () {
    return {
      timeMask: "#:##:##.#",
      workoutActivityData: [],
      selected: [],
      userWorkoutActivities: [],
      userWorkoutSets: [],
      userWorkouts: [],
      workoutTypes: [],
      workoutSetTypes: [],
      preEditWorkoutObject: {},
      distanceTypes: [],
      defaultRowsPerPage:[25, 50, 100, 200],
      workoutActivityRowsPerPage:[1000],
      notesRowsPerPage:[25],
      newWorkoutModal: false,
      newWorkoutActivityModal: false,
      newWorkoutSetModal: false,
      workoutNotesModal: false,
      showNewNoteTextField: false,
      expandWorkoutID: null,
      workoutNotesHeader: [
        {
          text: 'Create Date',
          align: 'left',
          sortable: false
        },
        {
          text: 'Created by',
          align: 'left',
          sortable: false
        },
        {
          text: 'Note',
          align: 'left',
          sortable: false
        }
      ],
      workoutHeader: [
        {
          text: 'Workout Description', 
          align: 'center',
          sortable: true,
          value: 'WorkoutDescription'
        },
        {
          text: 'Date Created',
          align: 'center',
          sortable: true,
          value: 'CreateDT'
        },
        {
          text: '',
          sortable: false
        }
      ],
      workoutSetHeader: [
        {
          text: 'Workout Type',
          align: 'center',
          sortable: false,
        },
        {
          text: 'Weight',
          align: 'center',
          sortable: false,
        },
        {
          text: 'Reps',
          align: 'center',
          sortable: false,
        },
        {
          text: 'Distance',
          align: 'center',
          sortable: false,
        },
        {
          text: 'Exercise Time',
          align: 'center',
          sortable: false,
        },
        {
          text: 'Split Time',
          align: 'center',
          sortable: false,
        },
        {
          text: 'Cadence',
          align: 'center',
          sortable: false,
        },
        {
          text: 'Rest Time',
          align: 'center',
          sortable: false,
        },
        {
          text: '',
          sortable: false,
        },
      ],
      workoutActivityHeader: [
        {
          text: 'Activity Description', 
          align: 'center',
          sortable: true,
          value: 'WorkoutActivityDescription',
        }
      ],
      newWorkout: {
        workoutDescription: '',
        workoutTypeID: 1
      },
      newWorkoutActivity:{
        workoutActivityDescription: '',
        workoutID: 0
      },
      newWorkoutSet:{
        workoutSetTypeID: 1,
        workoutSetDescription: "",
        workoutSetWeight: "",
        workoutSetReps: null,
        workoutSetExerciseTimeHours: null,
        workoutSetExerciseTimeMinutes: null,
        workoutSetExerciseTimeSeconds: null,
        workoutSetSplitTimeMinutes: null,
        workoutSetSplitTimeSeconds: null,
        workoutSetSPM: null,
        workoutSetRestTimeMinutes: null,
        workoutSetRestTimeSeconds: null,
        workoutSetWorkoutActivityID: null,
        workoutSetWorkoutID: null,
        workoutSetDistanceTypeID: 1,
        workoutSetDistance: null
      },
      workoutActivityPagination: {
        sortBy: 'WorkoutActivityID'
      },
      workoutNotesPagination: {
        sortBy: 'CreateDT',
        descending: true
      },
      workoutPagination: {
        sortBy: 'CreateDT',
        descending: true
      },
      workoutNotes: [],
      newWorkoutNote: {
        systemAreaID: 1,
        noteContent: "",
        forUserID: 0
      }
    }
  }, 
  methods: {
    addWorkout: function(event){
      generalAPICall('addWorkout', this.newWorkout).then(function(data){
        if(data == false){
          this.$parent.$emit('update-alert', "Failure", "Workout was not added, please try again later.");
          this.newWorkoutModal = false;
        } else{
          this.pullWorkoutActivityData().then(function(){
            this.expandWorkoutID = Math.max.apply(Math, this.userWorkouts.map(function(o){
              return o.WorkoutID;
            }));
            this.$refs.workoutRef.expanded[this.expandWorkoutID] = true;
            this.$forceUpdate();
          }.bind(this));
          this.$parent.$emit('update-alert', "Success", "Workout added successfully!");
          this.newWorkoutModal = false;
        }
      }.bind(this));
    },
    addWorkoutActivity: function(WorkoutID, ActivityDescription){
      this.newWorkoutActivity = {
        workoutActivityDescription: ActivityDescription,
        workoutID: WorkoutID
      }
      generalAPICall('addWorkoutActivity', this.newWorkoutActivity).then(function(data){
        if(data == false){
          this.$parent.$emit('update-alert', "Failure", "Activity was not added, please try again later.");
          this.newWorkoutActivityModal = false;
        } else{
          this.pullWorkoutActivityData().then(function(){
            this.expandWorkoutID = Math.max.apply(Math, this.userWorkouts.map(function(o){
              return o.WorkoutID;
            }));
            this.$refs.workoutRef.expanded[this.expandWorkoutID] = true;
            this.$forceUpdate();
          }.bind(this));
          this.$parent.$emit('update-alert', "Success", "Activity added successfully!");
          this.newWorkoutActivityModal = false;
        }
      }.bind(this));
    },
    addWorkoutSet: function(workoutActivityID, workoutID){
      if(this.newWorkoutSet.workoutSetWeight.replace(/\s/g,'') == ""){
        this.newWorkoutSet.workoutSetWeight = null;
      }
      this.newWorkoutSet.workoutSetWorkoutActivityID = workoutActivityID;
      this.newWorkoutSet.workoutSetWorkoutID = workoutID;
      generalAPICall('addWorkoutSet', this.newWorkoutSet).then(function(data){
        if(data == false){
          this.$parent.$emit('update-alert', "Failure", "Set was not added, please try again later.");
        } else {
          this.pullWorkoutActivityData().then(function(){
            this.$parent.$emit('update-alert', "Success", "Set added successfully!");
            this.newWorkoutSetModal = false;
            this.newWorkoutSet.workoutSetTypeID = 1,
            this.newWorkoutSet.workoutSetWeight = "",
            this.newWorkoutSet.workoutSetReps = null,
            this.newWorkoutSet.workoutSetExerciseTimeHours = null,
            this.newWorkoutSet.workoutSetExerciseTimeMinutes = null,
            this.newWorkoutSet.workoutSetExerciseTimeSeconds = null,
            this.newWorkoutSet.workoutSetSplitTimeMinutes = null,
            this.newWorkoutSet.workoutSetSplitTimeSeconds = null,
            this.newWorkoutSet.workoutSetSPM = null,
            this.newWorkoutSet.workoutSetRestTimeMinutes = null,
            this.newWorkoutSet.workoutSetRestTimeSeconds = null,
            this.newWorkoutSet.workoutSetWorkoutActivityID = null,
            this.newWorkoutSet.workoutSetWorkoutID = null
          }.bind(this));
        }
      }.bind(this));       
    },
    noWorkoutActivity: function(workoutID, props){
      let match = false;
      Object.keys(props).forEach((e) => {
        if(workoutID == props[e].WorkoutID){
          return true;
        }
      });
      return false;
    },
    showWorkoutSet: function(a, b){
      return a == b;
    },
    pullWorkoutActivityData: function(){
      var apiPromises = [];
      apiPromises.push(generalAPICall('getUserWorkoutActivities').then(function(data){
        this.userWorkoutActivities = data;
        let i = 0;
        while(i < this.userWorkoutActivities.length){
          this.$set(this.userWorkoutActivities[i], 'editBool', false);
//            this.$set(this.userWorkoutActivities[i], 'preEditValue', this.userWorkoutActivities[i].WorkoutActivityDescription);
          i++;
        }
        return true;
      }.bind(this)));
      apiPromises.push(generalAPICall('getUserWorkouts').then(function(data){
        this.userWorkouts = data;
        let i = 0;
        while(i < this.userWorkouts.length){
          this.userWorkouts[i].CreateDT = formatDateTime(this.userWorkouts[i].CreateDT);
          this.$set(this.userWorkouts[i], 'editBool', false);
//            this.$set(this.userWorkouts[i], 'preEditValue', this.userWorkouts[i].WorkoutDescription);
          i++;
        }
        return true;
      }.bind(this)));

      apiPromises.push(generalAPICall('getUserWorkoutSets').then(function(data){
        this.userWorkoutSets = data;
        let i = 0;
        while(i < this.userWorkoutSets.length){
          this.$set(this.userWorkoutSets[i], 'editBool', false);
          let exerciseTime = convertTimeHMS(this.userWorkoutSets[i].ExerciseTime);
          let restTime = convertTimeHMS(this.userWorkoutSets[i].RestTime);
          let splitTime = convertTimeHMS(this.userWorkoutSets[i].SplitTime);
          this.$set(this.userWorkoutSets[i], 'ExerciseTimeHours', exerciseTime[0]);
          this.$set(this.userWorkoutSets[i], 'ExerciseTimeMinutes', exerciseTime[1]);
          this.$set(this.userWorkoutSets[i], 'ExerciseTimeSeconds', exerciseTime[2]);
          this.$set(this.userWorkoutSets[i], 'RestTimeMinutes', restTime[1]);
          this.$set(this.userWorkoutSets[i], 'RestTimeSeconds', restTime[2]);
          this.$set(this.userWorkoutSets[i], 'SplitTimeMinutes', splitTime[1]);
          this.$set(this.userWorkoutSets[i], 'SplitTimeSeconds', splitTime[2]);
          let et = "" + exerciseTime[0] + exerciseTime[1] + exerciseTime[2];
          this.$set(this.userWorkoutSets[i], 'et', et);

          i++;
        }
        return true;
      }.bind(this)));

      if(this.workoutTypes.length == 0){
        apiPromises.push(generalAPICall('getWorkoutTypes').then(function(data){
          this.workoutTypes = data;
          this.newWorkout.workoutType = data[0];
          return true;
        }.bind(this)));
      }

      if(this.workoutSetTypes.length == 0){
        apiPromises.push(generalAPICall('getWorkoutSetTypes').then(function(data){
          this.workoutSetTypes = data;
          return true;
        }.bind(this)));
      }

      if(this.distanceTypes.length == 0){
        apiPromises.push(generalAPICall('getDistanceTypes').then(function(data){
          this.distanceTypes = data;
          return true;
        }.bind(this)));
      }

      return Promise.all(apiPromises).then(function(values){
        return values;
      });
    },
    expandWorkout: function(event, props){
      // Don't expand when one of the skipClasses are present
      let tClass = event.target.className;
      let skipClasses = ["input-group--selection-controls__ripple", "btn__content", "editIcon"];
      let expandActionIcon = tClass.includes("actionIcon") && props.expanded == true;
      if(skipClasses.some(el => tClass.includes(el)) || expandActionIcon){
        return false;
      }

      let path = event.path;
      path.forEach((currentValue) => {
        if(currentValue.tagName == "TR"){
          currentValue.classList.contains("expandedWorkout") ? currentValue.classList.remove("expandedWorkout") : 
          currentValue.classList.add("expandedWorkout");
        }
      });
      this.newWorkoutActivity = {
        workoutActivityDescription: '',
        workoutID: 0
      }
      props.expanded = !props.expanded
      this.$forceUpdate();
    },
    expandWorkoutSet: function(props){
      // Don't expand when one of the skipClasses are present
      let tClass = event.target.className;
      let skipClasses = ["actionIcon", "editIcon"];
      let expandActionIcon = tClass.includes("actionIcon") && props.expanded == true;
      if(skipClasses.some(el => tClass.includes(el)) || expandActionIcon){
        return false;
      }
      props.expanded = !props.expanded
    },
    addNewNote: function(){
      generalAPICall('addWorkoutNote', this.newWorkoutNote).then(function(data){
        if(data == false){
          this.$parent.$emit('update-alert', "Failure", "Note was not added, please try again later.");
        } else {
          this.$parent.$emit('update-alert', "Success", "Note was added successfully to your workout!");
        }
        this.showNewNoteTextField = false;
        this.workoutNotesModal = false;
      }.bind(this));
    },
    showCreateNoteForm: function(){
      this.showNewNoteTextField = true;
      this.$parent.$emit('reset-alerts');
    },
    showNewWorkoutModal: function(){
      this.newWorkout = { workoutTypeID: 1, workoutDescription: ''}
      this.$parent.$emit('reset-alerts');
      this.newWorkoutModal = true;
    },
    showNewWorkoutActivityModal: function(){
      this.newWorkoutActivityModal = !this.newWorkoutActivityModal;
      this.$parent.$emit('reset-alerts');
    },
    showNewWorkoutSetModal: function(){
      console.log("Show new workout set modal");
      console.log(this.newWorkoutSetModal);
      this.newWorkoutSetModal = !this.newWorkoutSetModal;
      console.log(this.newWorkoutSetModal);
      this.$parent.$emit('reset-alerts');
    },
    viewWorkoutNotes: function(event, workoutID){
      this.workoutNotes = [];
      this.newWorkoutNote.systemAreaID = workoutID;
      var workoutJSON = { workoutID: workoutID };
      generalAPICall('getWorkoutNotes', workoutJSON).then(function(data){
        this.workoutNotes = data;
        let i = 0;
        while(i < this.workoutNotes.length){
          this.workoutNotes[i].CreateDT = formatDateTime(this.workoutNotes[i].CreateDT);
          i++;
        }
        this.workoutNotesModal = true;
        this.$forceUpdate();
      }.bind(this))      
      return true;
    },
    editWorkout: function(systemArea, systemAreaKeyID, event=''){
      let index = 0;
      let returnValue = true;
      let workoutObject = {};
      let elementID = "edit" + systemArea + "TextField" + systemAreaKeyID;
      if(systemArea == "Workout"){
        workoutObject = this.userWorkouts;
      }
      if(systemArea == "WorkoutActivity"){
        workoutObject = this.userWorkoutActivities;
      }
      if(systemArea == "WorkoutSet"){
        workoutObject = this.userWorkoutSets;
      }
      //remove focus from element otherwise the values won't update on the DOM when even is a keystroke
      document.getElementById(elementID).blur();
      for (let i = 0; i < workoutObject.length; i++) {
        if(workoutObject[i][systemArea+"ID"] == systemAreaKeyID){
          index = i;
        }
      }

      if(workoutObject[index].editBool === false){
        this.preEditWorkoutObject = JSON.parse(JSON.stringify(workoutObject));
        returnValue = false;
      }

      workoutObject[index].editBool = !workoutObject[index].editBool;
      if(event == "esc"){
        this.$set(workoutObject, index, this.preEditWorkoutObject[index]);
        return false;
      }
        
      focusElement(elementID);
      if(JSON.stringify(this.preEditWorkoutObject[index]) == JSON.stringify(workoutObject[index])){
        returnValue = false;
      }

      if(returnValue === true){
        generalAPICall('update'+systemArea, workoutObject[index]).then((data) => {
          if(!data){
            workoutObject[index] = this.preEditWorkoutObject[index];
            this.$parent.$emit('update-alert', "Failure", systemArea.replace(/([a-z])([A-Z])/g, '$1 $2') + " was not updated, please try again later.");
          } else {
            this.$parent.$emit('update-alert', "Success", systemArea.replace(/([a-z])([A-Z])/g, '$1 $2') + " was updated successfully!");
          }
        });
      }
      return returnValue;
    }
  },
  mounted: function(){
    this.pullWorkoutActivityData();
  }
}